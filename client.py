import socket
SERVER_IP = "127.0.0.1"
SERVER_PORT = 5880
MAX_BYTES = 2048
COMMENDS_DICT = {1: "Get Albums", 2: "Get Album Songs", 3: "Get Song Length", 4: "Get Song Lyrics", 5: "Get Song Album", 6: "Search Song By Name", 7: "Search Song By Lyrics", 8: "Quit"}

def main():
	# Create a TCP/IP socket
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	
	# Connecting to remote computer 5880
	server_address = (SERVER_IP, SERVER_PORT)
	try:
		sock.connect(server_address)
	except Exception as e:
		print("Couldn't connect to server:", e)
		return
	
	
	if(send_password(sock)):
		return

	choice = 0
	while choice != 8:
		print_menu()
		try:
			choice = int(input("Enter number: ")) # verify input validness
		except Exception as e:
			print("Error:", e, "Please try again(numbers 1-8 only)")
			continue
		if choice > 8 or choice < 1:
			print("Please try again(numbers 1-8 only)")
			continue
			
		elif choice > 1 and choice < 8: # sending commends using the commends dict
			data = input("Enter data: ")
			msg = COMMENDS_DICT[choice] + ';' + data
			try: # checking if connection lost
				sock.sendall(msg.encode())
			except OSError as e:
				print("Sorry, connection lost: %s. Closing..." % e)
				sock.close()
				break
			
		elif choice == 1 or choice == 8: # commend 1, 8 don't need parameters
			msg = COMMENDS_DICT[choice]
			try: # checking if connection lost
				sock.sendall(msg.encode())
			except OSError as e:
				print("Sorry, connection lost: %s. Closing..." % e)
				sock.close()
				break
			
		server_msg = sock.recv(MAX_BYTES).decode() # server's respose
		server_msg = decode_protocol(server_msg)
		print("Server says:", server_msg)
	sock.close()
	

def send_password(sock):
	"""
	function repeatedly asking user for passwords in order to log into the server
	will keep procedure until either password guessed correctly or connection lost
	:param sock: the communication socket
	:type sock: socket.socket
	:return: True/False value, True when connection is lost and False when it isn'tell
	:rtype: bool
	"""
	server_msg = sock.recv(MAX_BYTES).decode() # server's asking for password, when guessed right server welcomes
	while server_msg != "Welcome!":
		print("Server says:", server_msg)
		password = input()
		try: # checking if connection lost
			sock.sendall(password.encode())
		except OSError as e:
			print("Sorry, connection lost: %s. Closing..." % e)
			sock.close()
			return True
		server_msg = sock.recv(MAX_BYTES).decode()
	
	print("Server says:", server_msg) # server's welcome
	return False
	
	
def decode_protocol(server_msg):
	"""
	function to decode protocol and return final messege
	:param server_msg: server's original messege
	:type server_msg: str
	:return: decoded final messege
	:rtype: str
	"""
	final_msg = ''
	for msg_part in server_msg.split(';'):
		final_msg += msg_part + ", "
	final_msg = final_msg[:-2]
	return final_msg
	
	
def print_menu():
	"""
	function to print user's menu
	:return: none
	"""
	print("""
	1. Get Albums
	2. Get Album Songs
	3. Get Song Length
	4. Get Song Lyrics
	5. Get Song Album
	6. Search Song By Name
	7. Search Song By Lyrics
	8. Quit""")
	
	
if __name__ == "__main__":
	main()
