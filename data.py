def get_dict(FILE_PATH):
	"""
	function to read and sort into dictionarys the necessary data from the pink floyed .txt file (like this: alboms --> songs --> properties(time and lyrics))
	:param FILE_PATH: the data file's path
	:type FILE_PATH: str
	:return: returns the final tripled dictionary which contains all the needed data for the server(returns data base)
	:rtype: dict
	"""
	file_obj = open(FILE_PATH, "r")
	file_data = file_obj.read()
	file_obj.close()
	
	data_dict = {}
	for album_data in file_data.split('#')[1:]:
		album_name = album_data.split("::")[0]
		data_dict[album_name] = {}
		for song_data in album_data.split('*')[1:]:
			song_name = song_data.split("::")[0]
			((data_dict[album_name])[song_name]) = {}
			((data_dict[album_name])[song_name])["time"] = song_data.split("::")[2]
			((data_dict[album_name])[song_name])["lyrics"] = song_data.split("::")[3]
	
	return data_dict

	
def Get_Albums(data_dict):
	"""
	function to return all pink floyed albums seperated by ';'(our protocol)
	:param data_dict: the dictionaried data base
	:type data_dict: dict
	:return: string contains all pink floyed albums seperated by ';'
	:rtype: str
	"""
	answer = ""
	for albom in data_dict:
		answer += albom + ';'
	return answer[:-1]
	
	
def Get_Album_Songs(data_dict, user_albom):
	"""
	function to return all songs in one of the pink floyed's album seperated by ';'(our protocol)
	:param data_dict: the dictionaried data base
	:param user_albom: user's choice of an albom to search
	:type data_dict: dict
	:type user_albom: str
	:return: all songs in the choosen pink floyed's album seperated by ';', or in case there is no such album, the following string: "No such albom, please try again"
	:rtype: str
	"""
	flag = False
	for albom in data_dict:
		if user_albom == albom:
			flag = True
			break
	if flag:
		answer = ""
		for song in data_dict[albom]:
			answer += song + ";"
		return answer[:-1]
	else:
		return "No such albom, please try again"
	
	
def Get_Song_Length(data_dict, user_song):
	"""
	function to return a song's length
	:param data_dict: the dictionaried data base
	:param user_song: user's choice of a song to search
	:type data_dict: dict
	:type user_song: str
	:return: length of the choosen pink floyed's song, or in case there is no such song, the following string: "No such song, please try again"
	:rtype: str
	"""
	flag = False
	for albom in data_dict:
		for song in data_dict[albom]:
			if user_song == song:
				flag = True
				break
		if flag:
			break
	if flag:
		return data_dict[albom][song]["time"]
	else:
		return "No such song, please try again"
	
	
def Get_Song_Lyrics(data_dict, user_song):
	"""
	function to return a song's lyrics
	:param data_dict: the dictionaried data base
	:param user_song: user's choice of a song to search
	:type data_dict: dict
	:type user_song: str
	:return: lyrics of the choosen pink floyed's song, or in case there is no such song, the following string: "No such song, please try again"
	:rtype: str
	"""
	flag = False
	for albom in data_dict:
		for song in data_dict[albom]:
			if user_song == song:
				flag = True
				break
		if flag:
			break
	if flag:
		return data_dict[albom][song]["lyrics"]
	else:
		return "No such song, please try again"
	
	
def Get_Song_Album(data_dict, user_song):
	"""
	function to return a song's album
	:param data_dict: the dictionaried data base
	:param user_song: the song the user wants to find its album
	:type data_dict: dict
	:type user_song: str
	:return: album of choosen pink floyed's song, or in case there is no such song, the following string: "No such song, please try again"
	:rtype: str
	"""
	flag = False
	for albom in data_dict:
		for song in data_dict[albom]:
			if user_song == song:
				flag = True
				break
		if flag:
			break
	if flag:
		return albom
	else:
		return "No such song, please try again"
	
	
def Search_Song_By_Name(data_dict, name):
	"""
	function to search a song(s) name containing given piece of string(case insensetive)
	:param data_dict: the dictionaried data base
	:param name: a name to search(a piece of string)
	:type data_dict: dict
	:type name: str
	:return: song(s) containing the name(seperated by ';'), or in case no song contains such string: "No song name containing such string"
	:rtype: str
	"""
	flag = False
	songs = ""
	for albom in data_dict:
		for song in data_dict[albom]:
			if name.lower() in song.lower():
				flag = True
				songs += song + ';'
	if flag:
		return songs[:-1]
	else:
		return "No song name containing such string"
	
	
def Search_Song_By_Lyrics(data_dict, lyrics):
	"""
	function to search a song(s) containing given piece of lyrics(case insensetive)
	:param data_dict: the dictionaried data base
	:param lyrics: lyrics to search
	:type data_dict: dict
	:type lyrics: str
	:return: song(s) containing the lyrics(seperated by ';'), or in case no song contains such lyrics: "No song containing such string"
	:rtype: str
	"""
	flag = False
	songs = ""
	for albom in data_dict:
		for song in data_dict[albom]:
			if lyrics.lower() in data_dict[albom][song]["lyrics"].lower():
				flag = True
				songs += song + ';'
	if flag:
		return songs[:-1]
	else:
		return "No song containing such string"