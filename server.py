import socket
import data
import hashlib
LISTEN_PORT = 5880
MAX_BYTES = 1024
FILE_PATH = "‏‏Pink_Floyd_DB.txt"
COMMENDS_DICT = {"Get Albums": data.Get_Albums, "Get Album Songs": data.Get_Album_Songs, "Get Song Length": data.Get_Song_Length, "Get Song Lyrics": data.Get_Song_Lyrics, "Get Song Album": data.Get_Song_Album, "Search Song By Name": data.Search_Song_By_Name, "Search Song By Lyrics": data.Search_Song_By_Lyrics}
PASSWORD = "2b1a04537eb6d8f8edc100e84ba5f6f5014b2f136200c26eee3c70e72a811a47" # password: YotamKing123

def main():
	data_dict = data.get_dict(FILE_PATH)
	 # print(data_dict)
	
	listening_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Create a TCP/IP socket
	
	server_address = ('', LISTEN_PORT) # Binding to local port 5880
	try:
		listening_sock.bind(server_address)
	except Exception as e:
		print("Error trying to bind...:", e)
		
	
	listening_sock.listen(1) # Listen for incoming connections
	
	while True:
		# Create a new conversation socket
		client_soc, client_address = listening_sock.accept()
		
		try: # checking if connection lost
			get_password(client_soc) # confirming user via password
		except OSError as e:
			print("client disconnected:", e)
			client_soc.close()
			continue
		client_soc.sendall("Welcome!".encode()) # sending welcome messege
		client_msg = ""
		while client_msg != "Quit":
			try: # checking if connection lost
				client_msg = client_soc.recv(MAX_BYTES).decode()
				print(client_msg)
			except OSError as e:
				print("client disconnected:", e)
				client_soc.close()
				break
			 # print("Client says:", client_msg) # remove first '#' in order to see user's requests
			if client_msg == "Get Albums": # in comperison to the other functions, this function don't use parameters and therefor the condition
				msg = COMMENDS_DICT[client_msg](data_dict)
				client_soc.sendall(msg.encode())
			elif client_msg == "Quit":
				Quit(client_soc)
			else:
				print(client_msg)
				msg = COMMENDS_DICT[client_msg.split(';')[0]](data_dict, client_msg.split(';')[1]) # calling functions using dictionary (cool, isn't it?) 
				client_soc.sendall(msg.encode())
	
	
def Quit(client_soc):
	"""
	function to say a goodbye messege and close client socket
	:param client_soc: the client socket
	:type client_soc: socket.socket
	:return: none
	"""
	client_soc.sendall("GoodBye!".encode())
	client_soc.close()
	
	
def get_password(client_soc):
	"""
	function to repeatedly ask user for password and compere to saved password(via its hash)
	:param client_soc: client socket
	:type client_soc: socket.socket
	:return: none
	"""
	client_soc.sendall("Please enter password: ".encode())
	client_password = client_soc.recv(MAX_BYTES).decode()
	hash_password = hashlib.sha256(client_password.encode()).hexdigest() # getting password's hash
	
	while hash_password != PASSWORD: # compering password's hash with our password(already hashed)
		client_soc.sendall("Wrong password, please try again: ".encode())
		client_password = client_soc.recv(MAX_BYTES).decode()
		
		hash_password = hashlib.sha256(client_password.encode()).hexdigest() # getting password's hash
	
	
if __name__ == "__main__":
	main()
